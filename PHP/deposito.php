<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="https://images.vexels.com/media/users/3/129288/isolated/lists/52e06e07244a3590366669665ea540e3-icone-de-circulo-do-banco-3.png">
    <title>Deposito</title>
</head>
<body background = "https://wallpaperaccess.com/full/340557.jpg">
    
    <form action="index.php" method="POST">
            <fieldset>
                <label>
                    <strong>Nome:</strong>
                    <br>
                    <input type="text" name="nome"/>
                </label>
                <br>
                <label>
                    <strong>Sobrenome:</strong>
                    <br>
                    <input type="text" name="sobrenome"/>
                </label>
                <br>
                <label>
                    <strong>Email:</strong>
                    <br>
                    <input type="text" name="email"/>
                </label>
                <br>
                <button type="submit" value="Cadastrar">Cadastrar</button>
            </fieldset>
        </form>
    <?php
        if(isset($_POST['email']) || !empty($_POST['email']))
        {
            $email = $_POST['email'];
            if(!filter_var($email, FILTER_VALIDATE_EMAIL))
                {
                    echo "informe um email valido" . "<br>";
                }
        }

        if(isset($_POST['sobrenome']) || !empty($_POST['sobrenome']))
        {
            $sobrenome = $_POST['sobrenome'];
            if(!preg_match("/^[a-zA-Z-']*$/",$sobrenome))
                {
                    echo "sobrenome invalido" . "<br>";
                }
        }
    ?>
</body>
</html>